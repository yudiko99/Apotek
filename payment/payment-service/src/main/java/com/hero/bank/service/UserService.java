package com.hero.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import com.hero.bank.dao.UserRepository;
import com.hero.bank.model.User;
import com.hero.bank.util.CollectionHelper;

@EnableCaching
@Service
public class UserService {

	@Autowired
	private UserRepository uRepository;
	
	@Cacheable(value="user.findAll", unless= "#result==null")
	@SuppressWarnings("unchecked")
	public List<User> findAll(){
		return CollectionHelper.iterToList(uRepository.findAll());
	}
	@CacheEvict(value="user.save", allEntries=true, beforeInvocation=true)
	public User save(User usr) {
		return uRepository.save(usr);
	}
	
	@Cacheable(value="user.login", unless= "#result==null")
	public User login(String nama, Integer pin) {
		return uRepository.findByNamaAndPin(nama, pin);
	}
}

