package com.hero.bank.dao;

import org.springframework.data.repository.CrudRepository;

import com.hero.bank.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public abstract User findByNamaAndPin(String nama, Integer pin);
}
