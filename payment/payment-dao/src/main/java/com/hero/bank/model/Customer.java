package com.hero.bank.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "m_customer")
public class Customer {
	@Id
	@Column(name = "customer_id", nullable = false, unique = true)
	private Integer customerId;

	@Column(name = "customer_name", nullable = false, length = 64)
	private String name;

	@Column(name = "customer_account", nullable = false, length = 64)
	private String account;

	@Column(name = "customer_address", nullable = false)
	private Integer address;
	
	@Column(name = "customer_balance", nullable = false)
	private Integer balance;
	
	@Column(name = "customer_phone", nullable = false)
	private String phone;
	
	@Column(name = "active_status", nullable = false)
	private String activeStatus;

	@OneToMany(targetEntity=Car.class, mappedBy = "owner", fetch=FetchType.LAZY)
	private List<Car> cars;

