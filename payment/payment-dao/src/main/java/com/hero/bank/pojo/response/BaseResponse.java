package com.hero.bank.pojo.response;

public class BaseResponse {

	private String ResponseCode;
	private String ResponseDesc;

	public String getResponseCode() {
		return ResponseCode;
	}

	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}

	public String getResponseDesc() {
		return ResponseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		ResponseDesc = responseDesc;
	}

	@Override
	public String toString() {
		return "BaseResponse [ResponseCode=" + ResponseCode + ", ResponseDesc=" + ResponseDesc + "]";
	}

}
